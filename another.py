# This is a simple demonstration on how to stream
# audio from microphone and then extract the pitch
# and volume directly with help of PyAudio and Aubio
# Python libraries. The PyAudio is used to interface
# the computer microphone. While the Aubio is used as
# a pitch detection object. There is also NumPy
# as well to convert format between PyAudio into
# the Aubio.
import aubio
import numpy as num
import pyaudio
import sys
import turtle
from math import sqrt

# Some constants for setting the PyAudio and the
# Aubio.
BUFFER_SIZE             = 2048
CHANNELS                = 1
FORMAT                  = pyaudio.paFloat32
METHOD                  = "default"
SAMPLE_RATE             = 44100
HOP_SIZE                = BUFFER_SIZE//2
PERIOD_SIZE_IN_FRAME    = HOP_SIZE

def main(args):
    # screen = Screen()
    # screen.setup(1000, 1000)
    screen = turtle.Screen()
    screen.setup (width=1920, height=1081, startx=None, starty=None)
    t = turtle.Turtle()
    # Initiating PyAudio object.
    pA = pyaudio.PyAudio()
    print(pA.get_device_info_by_index(2)['name'])
    # Open the microphone stream.
    mic = pA.open(format=FORMAT, channels=CHANNELS,
        input=True, rate=SAMPLE_RATE, input_device_index=2,
        frames_per_buffer=HOP_SIZE)

    # Initiating Aubio's pitch detection object.
    pDetection = aubio.pitch(METHOD, BUFFER_SIZE,
        HOP_SIZE, SAMPLE_RATE)
    # Set unit.
    pDetection.set_unit("Hz")
    # Frequency under -40 dB will considered
    # as a silence.
    pDetection.set_silence(-40)
    zero = True
    t.speed('fastest')
    # Infinite loop!
    i = 1
    j = 1
    l = 1
    k = 1
    coin = 0
    while True:

        # Always listening to the microphone.
	data = mic.read(PERIOD_SIZE_IN_FRAME, exception_on_overflow=False)
        # Convert into number that Aubio understand.
        samples = num.fromstring(data,
            dtype=aubio.float_type)
        # Finally get the pitch.
        pitch = pDetection(samples)[0]
        # Compute the energy (volume)
        # of the current frame.
        volume = num.sum(samples**2)/len(samples)
        # Format the volume output so it only
        # displays at most six numbers behind 0.
        volume = "{:6f}".format(volume)

        # print(pitch)
        if (t.xcor() < - 1920/2 or t.xcor() > 1920/2):
            t.penup()
            t.setpos(0,0)
            t.pendown()
        if (t.ycor() < -1080/2 or t.ycor() > 1080/2):
            t.penup()
            t.setpos(0,0)
            t.pendown()
        if (zero):
            if (abs(pitch) > 700 and abs(pitch)< 1032): # CCDDCEEG
                print('C')
                print(pitch)
                print(t.xcor())
                zero = False
                t.forward(100)
                t.right(90)
            elif (abs(pitch) > 1103 and abs(pitch)< 1250):
                print('D')
                print(pitch)
                print(t.xcor())
                zero = False
                t.forward(50)
                t.right(90)
            elif ((abs(pitch) > 1271 and abs(pitch)< 1350) or (abs(pitch) > 600 and abs(pitch)< 700)):
                print('E')
                print(pitch)
                print(t.xcor())
                zero = False
                t.forward(25)
                t.right(90)
            elif (abs(pitch) > 1350 and abs(pitch)< 1460):
                print('F')
                print(pitch)
                print(t.xcor())
                zero = False
                t.forward(25)
                t.right(90)
            elif (abs(pitch) > 1528 and abs(pitch)< 1580):
                print('G')
                print(pitch)
                print(t.xcor())
                zero = False
                t.forward(50)
            elif (abs(pitch) > 1719 and abs(pitch)< 1753): # combined with E two circles growing. 
                print('A')
                print(pitch)
                print(t.xcor())
                zero = False
                t.circle(6*i)
                i = i + 1
            elif (abs(pitch) > 1920 and abs(pitch)< 1993): # B G D draws a tiny house -> BBBBGD does the trick.
                print('B')
                print(pitch)
                print(t.xcor())
                zero = False
                t.forward(100)             
                t.right(90)
            elif (abs(pitch) > 2010 and abs(pitch)< 2155): # nautilus shape
                print('C')                
                print(pitch)
                print(t.xcor())
                index = 0
		while (index < 10):
			t.forward(k)
                	t.right(90)
                	if (k % 4 == 0):
                    		t.right(6)
                	k = k + 1
			index  = index + 1
                
            elif (abs(pitch) > 2200 and abs(pitch)< 2450):
                print('D')
                print(pitch)
                print(t.xcor())
                zero = False                
                t.right(90)
                t.forward(100 / sqrt(2))                                                 
            elif (abs(pitch) > 2500 and abs(pitch)< 2630):
                print('E')
                print(pitch)
                print(t.xcor())
                zero = False                
                t.circle(-6*i)
                t.left(i)
                i = i + 1
            elif (abs(pitch) > 2670 and abs(pitch)< 2782): # concentric circles from hexagons.
                print('F')
                print(pitch)
                print(t.xcor())
                index = 0
		while (index < 10):
                	t.forward(j)
                	t.left(60)
                	if (j % 3 == 0):
                    		t.left(3)
                	j = j + 1
			index = index + 1
            elif (abs(pitch) > 2940 and abs(pitch)< 3030): # G for tiny house 
                print('G')
                print(pitch)
                print(t.xcor())
                zero = False                
                t.left(45)
                t.forward(100 / sqrt(2))
            elif (abs(pitch) > 3280 and abs(pitch)< 3370): # spiral of squares
                print('A')
                print(pitch)
                print(t.xcor())
                index = 0
		while (index < 10):
			t.forward(l)
                	t.right(91)
                	l = l + 1
			index = index + 1
            elif (abs(pitch) > 3650 and abs(pitch)< 3870): # sometimes one must take a step backwards.
                print('B')
                print(pitch)
                print(t.xcor())
                t.backward(50)     
            elif (abs(pitch) > 3900 and abs(pitch)< 4100): # clear screen.
                print('C')
                print(pitch)
                print(t.xcor())
                i = 1
                j = 1
                l = 1
                k = 1
                turtle.clearscreen()
                t = turtle.Turtle()
                t.speed('fastest')
		
		if (coin == 0):
			screen.bgcolor("black")
			t.pencolor("white")
                	coin = 1
		else:
			screen.bgcolor("white")
			t.pencolor("black")
			coin = 0
		# t.setpos(0,0)
	#    else:
	#	print(pitch)
        elif(abs(pitch) < 700):
            zero = True
    	# Finally print the pitch and the volume.
            # print(str(pitch) + " " + str(volume))
        # turtle.done()

if __name__ == "__main__": main(sys.argv)
