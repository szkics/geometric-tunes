# This is a simple demonstration on how to stream
# audio from microphone and then extract the pitch
# and volume directly with help of PyAudio and Aubio
# Python libraries. The PyAudio is used to interface
# the computer microphone. While the Aubio is used as
# a pitch detection object. There is also NumPy
# as well to convert format between PyAudio into
# the Aubio.
import aubio
import numpy as num
import pyaudio
import sys
import turtle
from math import sqrt

# Some constants for setting the PyAudio and the
# Aubio.
BUFFER_SIZE             = 2048
CHANNELS                = 1
FORMAT                  = pyaudio.paFloat32
METHOD                  = "default"
SAMPLE_RATE             = 44100
HOP_SIZE                = BUFFER_SIZE//2
PERIOD_SIZE_IN_FRAME    = HOP_SIZE

def main(args):

    t = turtle.Turtle()
    # Initiating PyAudio object.
    pA = pyaudio.PyAudio()
    # Open the microphone stream.
    mic = pA.open(format=FORMAT, channels=CHANNELS,
        rate=SAMPLE_RATE, input=True,
        frames_per_buffer=PERIOD_SIZE_IN_FRAME)

    # Initiating Aubio's pitch detection object.
    pDetection = aubio.pitch(METHOD, BUFFER_SIZE,
        HOP_SIZE, SAMPLE_RATE)
    # Set unit.
    pDetection.set_unit("Hz")
    # Frequency under -40 dB will considered
    # as a silence.
    pDetection.set_silence(-40)
    zero = True
    # Infinite loop!
    t.speed(0)
    turtle.exitonclick()

    i = 0
    j = 0
    k = 0
    l = 0

    while True:
        # Always listening to the microphone.
        data = mic.read(PERIOD_SIZE_IN_FRAME)
        # Convert into number that Aubio understand.
        samples = num.fromstring(data,
            dtype=aubio.float_type)
        # Finally get the pitch.
        pitch = pDetection(samples)[0]
        # Compute the energy (volume)
        # of the current frame.
        volume = num.sum(samples**2)/len(samples)
        # Format the volume output so it only
        # displays at most six numbers behind 0.
        volume = "{:6f}".format(volume)
        # print(pitch)

        # if (i == 20):
        # 	i = 0
        # if (j == 20):
        # 	j = 0

        if (zero):
	        if (abs(pitch) > 700 and abs(pitch)< 1032): # CCDDCEEG
	            print('C')
	            print(pitch)
	            zero = False
	            t.forward(100)
	            t.right(90)
	        elif (abs(pitch) > 1103 and abs(pitch)< 1250):
	            print('D')
	            print(pitch)
	            zero = False
	            t.forward(50)
	            t.right(90)
	        elif ((abs(pitch) > 1271 and abs(pitch)< 1350) or (abs(pitch) > 600 and abs(pitch)< 700)):
	            print('E')
	            print(pitch)
	            zero = False
	            t.forward(25)
	            t.right(90)
	        elif (abs(pitch) > 1350 and abs(pitch)< 1460):
	            print('F')
	            print(pitch)            
	            zero = False
	            t.forward(25)
	            t.right(90)
	        elif (abs(pitch) > 1528 and abs(pitch)< 1580):
	            print('G')
	            print(pitch)
	            zero = False
	            t.forward(50)
	        elif (abs(pitch) > 1719 and abs(pitch)< 1753): # combined with E two circles growing. 
	            print('A')
	            print(pitch)
	            zero = False
	            t.circle(6*i)
	            # i = i + 1
	        elif (abs(pitch) > 1920 and abs(pitch)< 1993): # B C D draws a tiny house -> BBBBCD des the trick.
	            print('B')
	            print(pitch)
	            zero = False
	            t.right(90)
	            t.forward(100)             
	        elif (abs(pitch) > 2010 and abs(pitch)< 2155):
	            print('C')
	            print(pitch)            
	            zero = False	            
	            t.right(45)
	            t.forward(100 / sqrt(2))             	        
	        elif (abs(pitch) > 2200 and abs(pitch)< 2450):
	            print('D')
	            print(pitch)
	            zero = False	            
	            t.right(90)
	            t.forward(100 / sqrt(2))             	        	                     
	        elif (abs(pitch) > 2500 and abs(pitch)< 2630):
	            print('E')
	            print(pitch)
	            t.circle(-6*i)
	            t.left(i)
	            i = i + 1
	        elif (abs(pitch) > 2670 and abs(pitch)< 2782): # concentric circles from hexagons.
	            print('F')
	            print(pitch)            
	            for index in range (0,3):
        			t.forward(j)
        			t.left(60)
    				j = j + 1
				t.left(3) 
	        elif (abs(pitch) > 2940 and abs(pitch)< 3030): # nautilus shape 
	            print('G')
	            print(pitch)
	            for index in range(0,4):
	        		t.forward(k)
	        		t.right(90)
	        		k = k + 1
				t.right(6)
	        elif (abs(pitch) > 3280 and abs(pitch)< 3370): # spiral of squares
	            print('A')
	            print(pitch)
	            t.forward(l)
	            t.right(91)
	            l = l + 1
	        elif (abs(pitch) > 3650 and abs(pitch)< 3870): # sometimes one must take a step backwards.
	            print('B')
	            print(pitch)
	            t.backward(100)     
	        elif (abs(pitch) > 3900 and abs(pitch)< 4100): # clear screen.
	            print('C')
	            print(pitch)
	            t.clearscreen()
        elif (abs(pitch) < 500): # the time between two notes meaning a short period of silence. 
        	zero = True
       		print(pitch) 
       	else:
       		print(pitch)
        # Finally print the pitch and the volume.
        # print(str(pitch) + " " + str(volume))

    turtle.done()

if __name__ == "__main__": main(sys.argv)